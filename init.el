(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; https://www.reddit.com/r/emacs/comments/4j828f/til_setq_gcconsthreshold_100000000/
(setq gc-cons-threshold 100000000)
(add-hook 'after-init-hook (lambda () (setq gc-cons-threshold 800000)))

;; --------------------------------------------------------------------
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu"  . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "http://melpa.org/packages/")))
(package-initialize)
;; -------------------------------------------------------------------
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package exec-path-from-shell
  :ensure t
  :init (when (memq window-system '(mac ns))
          (exec-path-from-shell-initialize)))

(use-package no-littering
  :ensure t)

(require 'init-evil)
;; ivy------------------------------------------------------------------
(use-package ivy
  :ensure t
  :diminish ivy-mode
  :config
  (ivy-mode 1)
  (setq ivy-display-style 'fancy)
  (setq ivy-use-virtual-buffers t))
(use-package counsel
  :ensure t
  :init (setq ivy-initial-inputs-alist nil)  ;;important
  :bind
  (("C-s" . counsel-grep-or-swiper)
   ("M-x" . counsel-M-x)
   ("M-y"  . counsel-yank-pop)
   ("C-c h" . swiper-all)
   ("C-c k" . counsel-ag)
   ("C-c l" . counsel-git-log)
   ("C-x b" . ivy-switch-buffer)
   ("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep)
   ("C-c C-r" . ivy-resume)))

;; orgmode---------------------------------------------------------------
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :ensure org-plus-contrib)

(use-package org-bullets
  :ensure t
  :config (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
;; -----------------------------------------------------------------------
(use-package company
  :ensure t
  :diminish company-modes
  :config (global-company-mode))

(use-package company-c-headers
  :ensure t
  :config (add-to-list 'company-backends 'company-c-headers))

(use-package company-statistics
  :ensure t
  :config (company-statistics-mode))

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :config (add-hook 'prog-mode-hook #'yas-minor-mode))
;; ---------------------------------------------------------------------
;; python
(use-package anaconda-mode
  :ensure t
  :diminish anaconda-mode
  :config (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(use-package company-anaconda
  :ensure t
  :config (add-to-list 'company-backends '(company-anaconda :with company-capf)))
;; -----------------------------------------------------------------------
;; flycheck
(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(use-package flycheck-pos-tip
  :ensure t
  :config
  (with-eval-after-load 'flycheck
    (flycheck-pos-tip-mode)))
;; -----------------------------------------------------------------------
(use-package dash
  :ensure t)

(use-package popup
  :ensure t)

(use-package smex
  :ensure t)

(use-package hydra
  :ensure t)

(use-package zenburn-theme
  :ensure t)

(use-package expand-region
  :ensure t
  :bind ("C-c =" . er/expand-region))

(use-package beacon
  :ensure t
  :init (beacon-mode 1))

(use-package anzu
  :ensure t
  :diminish anzu-mode
  :config (global-anzu-mode)
  :bind
  (([remap query-replace] . anzu-query-replace)
   ([remap query-replace-regexp] . anzu-query-replace-regexp)))

(use-package slime
  :ensure t
  :config
  (setq inferior-lisp-program "sbcl")
  (setq slime-contribs '(slime-fancy)))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-prefix-prefix "*" )
  (setq which-key-idle-delay 0.4))

(use-package rainbow-mode
  :ensure t
  :diminish rainbow-mode
  :config (add-hook 'css-mode-hook #'rainbow-mode))

(use-package rainbow-delimiters
  :ensure t
  :diminish rainbow-mode
  :config (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package avy
  :ensure t
  :bind ("M-g c" . avy-goto-char-timer)
  ("M-g w" . avy-goto-word-1))

(use-package undo-tree
  :ensure t
  :config (global-undo-tree-mode)
  :diminish undo-tree-mode)

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init (projectile-mode)
  :config
  (setq projectile-completion-system'ivy)
  (setq projectile-enable-caching t))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (setq show-smartparens-mode 1)
  (add-hook 'prog-mode-hook #'smartparens-mode)
  ;;https://tuhdo.github.io/c-ide.html
  (sp-with-modes '(c-mode c++-mode)
    (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
    (sp-local-pair "/*" "*/" :post-handlers '((" | " "SPC")
                                              ("* ||\n[i]" "RET")))))

(use-package ace-window
  :ensure t
  :bind ("C-x o" . ace-window)
  :config
  (setq aw-keys   '(?a ?s ?d ?f ?j ?k ?l))
  (setq aw-dispatch-always t))


(use-package ggtags
  :ensure t
  :diminish ggtags-globalx-mode
  :config
  (setq-local eldoc-documentation-function #'ggtags-eldoc-function)
  (add-hook 'c-mode-common-hook
            (lambda ()
              (when (derived-mode-p 'c-mode 'c++-mode)
                (ggtags-mode 1)))))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; git---------------------------------------------------------------
(use-package magit
  :ensure t
  :config (set magit-completing-read-function 'ivy-completing-read)
  :bind
  ("C-x g" . magit-status)
  ("C-x G" . magit-dispatch-popup))

(use-package git-gutter
  :ensure t)

(use-package gitignore-mode
  :ensure)

(use-package gitconfig-mode
  :ensure)
;; ------------------------------------------------------------------
(load-theme 'zenburn t)

(diminish 'abbrev-mode)
;; -------------------------------------------------------------------
(require 'init-shell)
(require 'init-complete)
(require 'init-org)
(require 'init-hydra)
(require 'init-mode-line)
(require 'init-misc)
(require 'init-web-mode)


;; emacsclient
(require 'server)
(unless (server-running-p)
  (server-start))

;; custome.el
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))
