(eval-after-load 'company
  '(progn
     (add-to-list 'company-backends 'company-cmake)
     (setq company-backends (delete 'company-semantic company-backends))

     (setq company-show-numbers t)
     (setq company-etags-ignore-case t)
     ;; make Company mode be case-sensitive on plain Text
     (setq company-dabbrev-downcase nil)
     (setq company-idle-delay 0.2)

     (define-key company-active-map (kbd "M-n") nil)
     (define-key company-active-map (kbd "M-p") nil)
     (define-key company-active-map (kbd "C-n") #'company-select-next)
     (define-key company-active-map (kbd "C-p") #'company-select-previous)))


;; hippie-expand -------------------------------------------------------------------
(global-set-key (kbd "M-/") 'hippie-expand)

(setq hippie-expand-try-functions-list
      '(try-complete-file-name-partially
    try-complete-file-name
    try-expand-dabbrev
    try-expand-dabbrev-all-buffers
    try-expand-dabbrev-from-kill
    try-complete-lisp-symbol-partially
    try-complete-lisp-symbol))

(provide 'init-complete)
