(use-package evil
  :ensure t
  :config (evil-mode 1)
  (set 'ad-redefinition-action 'accept)
  (define-key evil-insert-state-map (kbd "C-e") 'move-end-of-line)
  (define-key evil-insert-state-map (kbd "C-a") 'move-beginning-of-line)
  (define-key evil-insert-state-map (kbd "C-k") 'kill-line)
  ;; vim's C-o in insert mode
  (define-key evil-insert-state-map (kbd "C-o") 'evil-execute-in-normal-state)
  ;; expand-region
  (define-key evil-visual-state-map (kbd "=") 'er/expand-region)

  ;; org-mode
  (with-eval-after-load 'org
    (define-key evil-insert-state-map (kbd "TAB") 'org-cycle)
    (define-key evil-normal-state-map (kbd "TAB") 'org-cycle)))


(use-package evil-leader
  :ensure t
  :diminish evil-leader-mode
  :config (global-evil-leader-mode)
  (evil-leader/set-leader ",")
  (evil-leader/set-key
    "."  'counsel-M-x
    "f"  'hydra-flycheck/body
    ","  'avy-goto-char-timer))

(use-package evil-surround
  :ensure t
  :diminish evil-surround-mode
  :config (global-evil-surround-mode 1))

(provide 'init-evil)
