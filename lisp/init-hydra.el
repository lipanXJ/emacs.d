;; https://github.com/abo-abo/ace-window/wiki
;; ace-window
(defhydra hydra-window-size (:color red)
  "Windows size"
  ("h" shrink-window-horizontally "shrink horizontal")
  ("j" shrink-window "shrink vertical")
  ("k" enlarge-window "enlarge vertical")
  ("l" enlarge-window-horizontally "enlarge horizontal")
  ("f" make-frame "new frame")
  ("x" delete-frame "delete frame"))

;; flycheck
(defhydra hydra-flycheck
  (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
	:post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
	:hint nil)
  "Errors"
  ("f"  flycheck-error-list-set-filter                            "Filter")
  ("j"  flycheck-next-error                                       "Next")
  ("k"  flycheck-previous-error                                   "Previous")
  ("h" flycheck-first-error                                      "First")
  ("l"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("q"  nil))


(provide 'init-hydra)
