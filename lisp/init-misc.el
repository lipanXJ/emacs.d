(menu-bar-no-scroll-bar)
(menu-bar-mode -1)
(tool-bar-mode -1)
(winner-mode 1)
(which-function-mode)
(save-place-mode 1)

(global-set-key [f12] 'menu-bar-mode)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-c s") 'shell)
(global-set-key (kbd "M-V") 'scroll-other-window-down)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(define-key prog-mode-map [f5] #'compile)

;; cleanup whitespace
(add-hook 'before-save-hook 'whitespace-cleanup)

(setq inhibit-startup-screen t)

;; gui-frame
(setq initial-frame-alist '( (width . 100) (height . 42)))

;; disable bell
(setq visible-bell t)
(setq ring-bell-function 'ignore)

;; recentf-mode
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 200)
(global-set-key (kbd "C-x f") 'counsel-recentf)

;; turn off warning when open large file
(setq large-file-warning-threshold nil)

;; time 24h-mode
(setq display-time-default-load-average nil)
(setq display-time-24hr-format t)
(display-time)

;; auto refresh buffer
(global-auto-revert-mode t)

;; indent style
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)
(setq c-default-style "linux")
(setq python-indent-guess-indent-offset nil)

;; disable backup file
(setq make-backup-files nil)

;; auto-save-file
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(setq mouse-drag-copy-region t)

;; using y/n replace yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; Nicer naming of buffers for files with identical names
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; compile
(setq compilation-ask-about-save nil)
(setq compilation-scroll-output 'next-error)
(setq compilation-skip-threshold 2)

;; -------------------------------------------------------------------
;; http://endlessparentheses.com/faster-pop-to-mark-command.html
(defun modi/multi-pop-to-mark (orig-fun &rest args)
  "Call ORIG-FUN until the cursor moves.
Try the repeated popping up to 10 times."
  (let ((p (point)))
    (dotimes (i 10)
      (when (= p (point))
        (apply orig-fun args)))))
(advice-add 'pop-to-mark-command :around
            #'modi/multi-pop-to-mark)

(setq set-mark-command-repeat-pop t)




(provide 'init-misc)
