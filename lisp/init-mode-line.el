;; http://www.lunaryorn.com/posts/new-mode-line-support-in-flycheck.html
(setq flycheck-mode-line
      '(:eval
    (pcase flycheck-last-status-change
      (`not-checked nil)
      (`no-checker (propertize " -" 'face 'warning))
      (`running (propertize " ✷" 'face 'success))
      (`errored (propertize " !" 'face 'error))
      (`finished
       (let* ((error-counts (flycheck-count-errors flycheck-current-errors))
          (no-errors (cdr (assq 'error error-counts)))
          (no-warnings (cdr (assq 'warning error-counts)))
          (face (cond (no-errors 'error)
                  (no-warnings 'warning)
                  (t 'success))))
         (propertize (format "{%s:%s}" (or no-errors 0) (or no-warnings 0))
             'face face)))
      (`interrupted " -")
      (`suspicious '(propertize " ?" 'face 'warning)))))


;; https://emacs-china.org/t/mode-line/655
(setq-default mode-line-format
          (list

           "%1 "
           ;; the buffer name; the file name as a tool tip
           '(:eval (propertize "%b " 'face 'font-lock-keyword-face
                   'help-echo (buffer-file-name)))

           ;; line and column
           "(" ;; '%02' to set to 2 chars at least; prevents flickering
           (propertize "%02l" 'face 'font-lock-type-face) ":"
           (propertize "%02c" 'face 'font-lock-type-face)
           ") "
           ;; relative position, size of file
           "["
           (propertize "%p" 'face 'font-lock-constant-face) ;; % above top
           "/"
           (propertize "%I" 'face 'font-lock-constant-face) ;; size
           "]"

           ;; flycheck
           "%1 "
           'flycheck-mode-line  ;;init-flycheck.el
           "%1"

           ;; evil state
           '(:eval evil-mode-line-tag)

           ;; minor modes
           ;; 'minor-mode-alist
           ;; ""

           'mode-line-misc-info

           ;; the current major mode for the buffer.
           "["
           '(:eval (propertize "%m" 'face 'font-lock-string-face
                   'help-echo buffer-file-coding-system))
           "]"

           ;; git info
           `(vc-mode vc-mode)


           ))

(provide 'init-mode-line)
