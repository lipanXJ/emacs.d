(setq  org-src-fontify-natively t
       org-src-tab-acts-natively t
       org-confirm-babel-evaluate nil ;execute code without confirm
       org-hide-emphasis-markers t    ; Hide  markup characters, "/text/" becomes "text"
       )

;; https://github.com/emacs-china/hello-emacs/blob/master/emacs-faq.org
(defun endless/org-ispell ()
  "Configure `ispell-skip-region-alist' for `org-mode'."
  (make-local-variable 'ispell-skip-region-alist)
  (add-to-list 'ispell-skip-region-alist '(org-property-drawer-re))
  (add-to-list 'ispell-skip-region-alist '("~" "~"))
  (add-to-list 'ispell-skip-region-alist '("=" "="))
  (add-to-list 'ispell-skip-region-alist '("^#\\+BEGIN_SRC" . "^#\\+END_SRC")))
(add-hook 'org-mode-hook #'endless/org-ispell)


(provide 'init-org)
